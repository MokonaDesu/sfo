library ieee;

entity a2 is
  port(
    addr: in bit_vector (1 downto 0);
    actual: in bit;
    update: in bit;
    prediction: out bit := '0'
  );
end entity;

architecture behv of a2 is
  signal pht_00, pht_01, pht_10, pht_11 : bit_vector (1 downto 0);

begin
  -- updates the PHT based on the current state and actual bit
  -- that comes from external circuitry
  pht_update: process(update)
  begin
    case addr is
      when "00" => pht_00 <= pht_00(0) & actual;
      when "01" => pht_01 <= pht_01(0) & actual;
      when "10" => pht_10 <= pht_10(0) & actual;
      when "11" => pht_11 <= pht_11(0) & actual;
    end case;
  end process;

  -- updates the current prediction output based on the
  -- PHT state
  prediction_update: process(addr, pht_00, pht_01, pht_10, pht_11)
  begin
    case addr is
      when "00" => prediction <= pht_00(0) or pht_00(1);
      when "01" => prediction <= pht_01(0) or pht_01(1);
      when "10" => prediction <= pht_10(0) or pht_10(1);
      when "11" => prediction <= pht_11(0) or pht_11(1);
    end case;
  end process;
end architecture;
