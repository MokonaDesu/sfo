library ieee;

entity a1 is
  port(
    addr: in bit;
    update: in bit;
    actual: in bit;
    prediction: out bit := '0'
  );
end entity;

architecture behv of a1 is
  signal pht_0: bit := '0';
  signal pht_1: bit := '0';

begin
  -- updates PHT based on the actual value that comes
  -- from external circuitry
  pht_update: process(update)
  begin
    if update = '1' then
      if addr = '0' then
        pht_0 <= actual;
      else
        pht_1 <= actual;
      end if;
    end if;
  end process;

  -- updates the current output based on the current
  -- PC port value
  prediction_update: process(addr, pht_0, pht_1)
  begin
    if addr = '0' then
      prediction <= pht_0;
    else
      prediction <= pht_1;
    end if;
  end process;
end architecture;
