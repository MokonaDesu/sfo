library ieee;

entity test_bench is
end entity;

architecture behv of test_bench is
  signal update: bit := '0';
  signal pc: bit := '0';
  signal actual: bit := '0';
  signal prediction: bit := '0';

begin
  predictor_module: entity work.predictor(behv)
    port map(pc, actual, update, prediction);
  
  stimulus: process
  begin
      pc <= '0';
      wait for 100 ps;
      actual <= '1';
      update <= '1';
      wait for 100 ps;
      update <= '0';
      actual <= '1';
      pc <= '1';
      wait for 100 ps;
      pc <= '0';
      wait for 100 ps;
      actual <= '0';
      update <= '1';
      wait for 100 ps;
      update <= '0';
      wait for 300 ps;
      pc <= '1';
      wait for 100 ps;
      pc <= '0';
      wait for 1 ns;
  end process;
      
end architecture;
