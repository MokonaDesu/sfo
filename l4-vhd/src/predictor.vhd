library ieee;

entity predictor is
  port(
    pc: in bit;
    actual: in bit;
    update: in bit;
    prediction: out bit := '0'
  );
end entity;

architecture behv of predictor is
  signal ghr_val: bit_vector (1 downto 0);
  signal a1_out: bit;
  signal a2_out: bit;

begin
  a1_module: entity work.a1(behv)
    port map(pc, update, actual, a1_out);

  ghr_module: entity work.ghr(behv)
    port map(actual, update, ghr_val);
  
  a2_module: entity work.a2(behv)
    port map(ghr_val, actual, update, a2_out);
  
  out_update: process(a1_out, a2_out) is
  begin
    prediction <= a1_out or a2_out;
  end process;
end architecture;
