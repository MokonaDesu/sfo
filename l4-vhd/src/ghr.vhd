library ieee;

entity ghr is
  port(
    actual: in bit;
    update: in bit;
    addr: out bit_vector(1 downto 0)
  );
end entity;

architecture behv of ghr is
  signal addr_reg: bit_vector(1 downto 0) := "00";
  
begin
  output_update: process(update)
  begin
    if update = '1' then
      addr_reg <= addr_reg(0) & actual;
      addr <= addr_reg(0) & actual;
    end if;
  end process;
end architecture;
